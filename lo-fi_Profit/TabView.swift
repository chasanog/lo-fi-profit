//
//  TabView.swift
//  lo-fi_Profit
//
//  Created by Micaela Cavallo on 11/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

//struct TabView: View {
//    @Binding var index : Int
//    
//    var body : some View {
//        
//        HStack {
//            Button(action: {
//                self.index = 0
//            }) {
//                VStack {
//                    Image(systemName: "doc.richtext").scaleEffect(1.5)
//                        .padding(.top, 16)
//                        .padding(.bottom, 10)
//                    Text("Stories").font(.footnote)
//                }
//                
//            }
//            .foregroundColor(Color.black.opacity(self.index == 0 ? 1 : 0.4))
//            Spacer(minLength: 0)
//            
//            Button(action: {
//                self.index = 1
//                
//            }) {
//                VStack {
//                    Image(systemName: "map.fill").scaleEffect(1.5)
//                        .padding(.top, 16)
//                        .padding(.bottom, 10)
//                    Text("Explore").font(.footnote)
//                }
//            }.foregroundColor(Color.black.opacity(self.index == 1 ? 1 : 0.4))
//            Spacer(minLength: 0)
//            
//            Button(action: {
//                self.index = 2
//            }) {
//                VStack {
//                    Image(systemName: "person.fill").scaleEffect(1.5)
//                        .padding(.top, 16)
//                        .padding(.bottom, 10)
//                    Text("Profile").font(.footnote)
//                }
//            }
//            .foregroundColor(Color.black.opacity(self.index == 2 ? 2 : 0.4))
//            Spacer(minLength: 0)
//            
//            Button(action: {
//                self.index = 3
//            }) {
//                VStack {
//                    Image(systemName: "ellipsis").scaleEffect(1.5)
//                        .padding(.top, 16)
//                        .padding(.bottom, 10)
//                    Text("More").font(.footnote)
//                }
//            }
//            .foregroundColor(Color.black.opacity(self.index == 3 ? 1 : 0.4))
//            
//        }
//        .padding(.horizontal, 35)
//        .frame(maxHeight: 80)
//        .background(Color.white)
//    }
//}




