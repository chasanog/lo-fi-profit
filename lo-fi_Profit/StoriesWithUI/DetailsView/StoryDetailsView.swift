//
//  StoryDetailsView.swift
//  lo-fi_Profit
//
//  Created by Micaela Cavallo on 09/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct StoryDetailsView: View {
    
    @Binding var isPresented : Bool
    @ObservedObject var storyCellVM: StoryCellViewModel
    @Environment(\.imageCache) var cache: ImageCache
    
    private let text = "Cronus, having an uneasy con- science, was afraid that his children might one day rise up against his author- ity, and thus verify the prediction of his father Uranus. In order, therefore, to render the prophecy impossible of fulfilment, Cronus swallowed each child as soon as it was born,1 greatly to the sorrow and indignation of his wife Rhea. When it came to Zeus, the sixth and last, Rhea resolved to try and save this one child at least, to love and cherish, and appealed to her parents, Uranus and Gæa, for counsel and assistance. By their advice she wrapped a stone in baby-clothes, and Cronus, in eager haste, swallowed it, without noticing the deception. The child thus saved, eventually, as we shall see, dethroned his fa- ther Cronus, became supreme god in his stead, and was universally venerated as the great national god of the Greeks.Anxious to preserve the secret of his existence from Cronus, Rhea sent the infant Zeus secretly to Crete, where he was nourished, protected, and educated. A sacred goat, called Amalthea, supplied the place of his mother, by providing him with milk; nymphs, called Melissae, fed him with honey, and eagles and doves brought him nectar and ambrosia. He was kept conceiled in a cave in the heart of Mount Ida, and the Curetes, or priests of Rhea, by beating their shields together, kept up a constant noise at the entrance, which drowned the cries of the child and frightened away all intruders. Under the watchful care of the Nymphs the infant Zeus throve rapidly, developing great physical powers, combined with extraordinary wisdom and intelligence. Grown to manhood, he determined to compel his father to restore his brothers and sisters to the light of day"
    
    
    
    //create a new substring from a string, choosing length by range of its words' indexes (instead of character index)
    func textSubselection(originalText: String, indexStart : Int, indexEnd: Int?, end: Bool) -> String {
        let componentsArray = rangeOfComponents(text: originalText, from: indexStart, to: indexEnd, end: end)
        let reJoinedString = stringSelectedComponentRange(stringArray: componentsArray)
        return reJoinedString
    }
    //subfunction1 for textSubselection function
    //break a string into a string array.
    func rangeOfComponents(text: String, from : Int, to: Int?, end: Bool) -> [String] {
        let array = text.components(separatedBy: " ")
        if end {
            let subArray = array[from...array.count - 1]
            return Array(subArray)
        } else if !end && to != nil {
            let subArray = array[from...to!]
            return (Array(subArray))
        } else {
            return ["There was an error. Please enter an end index or declare the end variable to be true."]
        }
    }
    //subfunction2 for textSubselection function
    //convert the string array into a string.
    func stringSelectedComponentRange(stringArray : [String])  -> String {
        let joined = stringArray.joined(separator: " ")
        return joined
    }
    func shareButton() {
        let url = URL(string: "https://apple.com")
        let av = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
        
        UIApplication.shared.windows.first?.rootViewController?.present(av, animated: true)
    }
    
    var body: some View {
        ScrollView {
            VStack(spacing: 0) {
                
                ZStack {
                    AsyncImage(url: URL(string:
                    storyCellVM.story.image!)!, cache: self.cache, placeholder: Text("loading..."))
//                    Image("typewriter")
//                        .resizable()
                        .scaledToFill()
                        .frame(height: 250, alignment: .center)
                        .clipped()
                    
                    VStack {
                        HStack {
                            Spacer()
                            Button(action: {
                                print("button pressed")
                                self.isPresented = false
                            }) {
                                Image(systemName: "xmark").font(.title).foregroundColor(.white)
                            }
                            .frame(width: 45, height: 45)
                            .background(Color.gray .opacity(0.8))
                            .cornerRadius(22)
                            .padding()
                            
                        }
                        Spacer()
                        Rectangle().frame(height: 60).foregroundColor(.black)
                    }
                    AuthorNameOverlay(author: "\(storyCellVM.story.author ?? "Unknown")")
                        .padding(.trailing, 4)
                        .padding(.bottom, 10)
                    
                    VStack {
                        Spacer()
                        
                        HStack {
                            Text(storyCellVM.story.title).font(.largeTitle).fontWeight(.medium).foregroundColor(.white)
                            .padding(12)
//                            Text("Story Title").font(.largeTitle).fontWeight(.medium).foregroundColor(.white)
//                                .padding(12)
                            Spacer()
                        }
                    }
                    
                    
                }
                
                ZStack {
                    Rectangle().frame(height: 60).foregroundColor(.green)
                    HStack {
                        VStack {
                            Text("#\(storyCellVM.story.tag ?? "notSet")").font(.callout).fontWeight(.medium).foregroundColor(.black)
//                            Text("#tag #tag").font(.callout).fontWeight(.medium).foregroundColor(.black)
                            Text("category").font(.callout).fontWeight(.medium).foregroundColor(.black)
                        }.padding(.horizontal, 10)
                        Spacer()
                        
                        Button(action: {
                            print("button pressed")
                            // self.shareButton()
                            //cannot call this modal from a modal - investigate
                        }) {
                            Image(systemName: "square.and.arrow.up").font(.headline).foregroundColor(Color.white.opacity(0.9))
                        }
                        .frame(width: 90, height: 40)
                        .background(Color.black)
                        .cornerRadius(22)
                        .padding(.trailing, 12)
                    }
                    
                }
                VStack {
//                    Rectangle().foregroundColor(.clear).frame( height: 14)
                    ZStack {
//                        Rectangle()
                            //                            .foregroundColor(Color.red.opacity(0.2))
//                            .foregroundColor(Color.white.opacity(0.2))
                        Text(storyCellVM.story.description).font(.body).foregroundColor(.black)
//                        Text(textSubselection(originalText: text, indexStart: 0, indexEnd: 107, end: false)).font(.body).foregroundColor(.black)
                    }.frame(width: 350, height: 360)
                    //
                    //).background(Color.white)
                    //  .frame(minWidth: 300, idealWidth: 320, maxWidth: 330, minHeight: 280, idealHeight: 700, maxHeight: 800, alignment: .center)
                    //  .padding()
                    AsyncImage(url: URL(string:
                    storyCellVM.story.businessImage!)!, cache: self.cache, placeholder: Text("loading..."))
//                    Image("happyDog")
//                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 350, height: 260, alignment: .center)
                        .clipped()
                        .cornerRadius(16)
                    
//                    ZStack {
////                        Rectangle()
//                            //                            .foregroundColor(Color.red.opacity(0.2))
////                            .foregroundColor(Color.white.opacity(0.2))
//                        Text(textSubselection(originalText: text, indexStart: 108, indexEnd: nil, end: true)).font(.body).foregroundColor(.black)
//                    }.frame(width: 350, height: 360)
                    
                    ZStack {
                        Rectangle().foregroundColor(.green)
                        
                        HStack {
                            Spacer()
                            Text("Find us on the map !").font(.headline).foregroundColor(Color.black.opacity(0.7)).padding(.leading, 20)
                            Spacer()
                            ShopProfileZStack(authorPic: "happyDog")
                                .padding()
                        }
                    }.frame(width: 350, height: 70)
                        .cornerRadius(28)
                    
                    
                }
                Spacer()
            }
        }.edgesIgnoringSafeArea(.top)
    }
}

//struct StoryDetailsView_Previews: PreviewProvider {
//    static var previews: some View {
//        StoryDetailsView(isPresented: true)
//    }
//}
