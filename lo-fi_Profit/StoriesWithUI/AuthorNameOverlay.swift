//
//  AuthorNameOverlay.swift
//  Flava
//
//  Created by Micaela Cavallo on 07/06/2020.
//  Copyright © 2020 cavallo.classes. All rights reserved.
//

import SwiftUI

struct AuthorNameOverlay: View {
    var author: String
    var body: some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                Text("By: \(author)")
                    .foregroundColor(.black).font(.footnote).fontWeight(.medium)
                    .padding(8)
                    .background(Color.white.opacity(0.8))
                    .cornerRadius(12)
                
            }.padding(5)
    }
}
}
struct AuthorNameOverlay_Previews: PreviewProvider {
    static let name = "Shakespeare"
    static var previews: some View {
        VStack {
            AuthorNameOverlay(author: name)
        }.background(Color.black)
        
    }
}
