//
//  OldStoryListCard.swift
//  lo-fi_Profit
//
//  Created by Micaela Cavallo on 13/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//
//  OldStoryCard.swift
//  lo-fi_Profit
//
//  Created by Micaela Cavallo on 13/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct OldStoryListCard: View {
    
    @ObservedObject var storyCellVM: StoryCellViewModel
    @Environment(\.imageCache) var cache: ImageCache
    @State var presentDetailsModal = false
    
    //let imagesURLstring = [String]()
    
    func cardSubstring(text: String, index: Int) -> String {
            if text.count > index {
                let substring = text.prefix(index)
                return String(substring)
            } else {
                return text
            }
        }
       var body: some View {
        
        VStack(alignment: .leading, spacing: 0) {
            
            ZStack {
                
                if storyCellVM.story.image != nil {
                    AsyncImage(url: URL(string:
                        storyCellVM.story.image!)!, cache: self.cache, placeholder: Text("loading..."))
                        //                    .resizable()
                        .aspectRatio(contentMode: .fill)
                        
                        .frame(maxHeight: 250)
                        .clipShape(Rectangle())
                }
                
                AuthorNameOverlay(author: storyCellVM.story.author ?? "anonymous")
            }
            
            VStack(alignment: .leading, spacing: 0) {
                Text(storyCellVM.story.title)
                    .font(.title)
                    .padding(.horizontal)
                    .padding(.top, 14)
                    .padding(.bottom, 25)
                
                Text(storyCellVM.story.description)
                    .font(.body)
                    .opacity(0.8)
                    .padding(.horizontal)
                
                HStack {
                    VStack {
                        
                        Text("#\(storyCellVM.story.tag ?? "notSet")")
                            .font(.footnote).fontWeight(.bold)
                            .padding(.horizontal, 17)
                            .offset(x: 0, y: 22)
                    }
                    Spacer()
                    VStack(spacing: 0) {
                        
                        
                        ZStack {
                            
                            Circle().foregroundColor(.pink)
                                .frame(width: 40, height: 40)
                            
                            if storyCellVM.story.businessImage != nil {
                                AsyncImage(url: URL(string:
                                    storyCellVM.story.businessImage!)!, cache: self.cache, placeholder: Text("loading..."))
                                    .aspectRatio(contentMode: .fill)
                                    .frame(width: 35, height: 35)
                                    .clipShape(Circle())
                            }
                            
                        }.padding(5)
                        
                        VStack(alignment: .center, spacing: 0) {
                            
                            Text(storyCellVM.story.location ?? "not set").font(.caption)
                        }
                    }
                        
                    .padding(.trailing, 12)
                    
                }
                //.padding(.top, 12)
                
                
                
            }.frame(height: 240)
        }
        .cornerRadius(12, corners: [.topLeft, .topRight])
        .background(
            RoundedRectangle(cornerRadius: 12)
                
                .foregroundColor(.white)
                .shadow(
                    color: Color.gray,
                    radius: 4,
                    x: 2,
                    y: 2
            )
        )
            
            .frame(width: 340, height: 470)
            .padding()
            .cornerRadius(8)
            .onTapGesture(perform: {
                self.presentDetailsModal.toggle()
            })
            .sheet(isPresented: $presentDetailsModal) {
                StoryDetailsView(isPresented: self.$presentDetailsModal, storyCellVM: self.storyCellVM)
        }
    }
}

//struct OldStoryListCard_Previews: PreviewProvider {
//    static var previews: some View {
//        OldStoryCard()
//    }
//}
