//
//  ShopProfileZStack.swift
//  Flava
//
//  Created by Micaela Cavallo on 07/06/2020.
//  Copyright © 2020 cavallo.classes. All rights reserved.
//

import SwiftUI

struct ShopProfileZStack: View {
    var authorPic : String
    var body: some View {
        ZStack {
            
            Circle().foregroundColor(.pink)
                .frame(width: 40, height: 40)
            Image(authorPic)
            .brCircle(width: 35)
        }
    }
}

struct ShopProfileZStack_Previews: PreviewProvider {
    static let authorPic = "happyDog"
    static var previews: some View {
        ShopProfileZStack(authorPic: authorPic)
    }
}

