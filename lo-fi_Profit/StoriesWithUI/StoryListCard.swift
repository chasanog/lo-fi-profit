
//
//  StoryListCard.swift
//  lo-fi_Profit
//
//  Created by Micaela Cavallo on 04/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//


import SwiftUI

struct StoryListCard: View {
    @ObservedObject var storyCellVM: StoryCellViewModel
    @Environment(\.imageCache) var cache: ImageCache
    @State var presentDetailsModal = false
    
    //let imagesURLstring = [String]()
    
    func cardSubstring(text: String, index: Int) -> String {
            if text.count > index {
                let substring = text.prefix(index)
                return String(substring)
            } else {
                return text
            }
        }
        
        
    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .leading, spacing: 0) {
                
                ZStack {
                    
                    if self.storyCellVM.story.image != nil {
                        AsyncImage(url: URL(string:
                            self.storyCellVM.story.image!)!, cache: self.cache, placeholder: Text("loading..."))
                            //                    .resizable()
                            .aspectRatio(1.0, contentMode: .fill)
                            .frame(maxHeight: geometry.size.height * 0.5)
                            .clipShape(Rectangle())
                    }
                    
                    AuthorNameOverlay(author: self.storyCellVM.story.author ?? "anonymous")
                }
                
                VStack(alignment: .leading, spacing: 0) {
                    
                    Text(self.storyCellVM.story.title)
                        .font(.title)
                        .padding(.horizontal)
                        .padding(.top, 14)
                        .padding(.bottom, 25)
                    
    //                Text("\(cardSubstring(text: storyCellVM.story.description, index: 100)) ... ")
                    Text(self.storyCellVM.story.description)
                        .font(.body)
                        .opacity(0.8)
                        .padding(.horizontal)
                    
                    HStack {
                        VStack {
                            
                            Text("#\(self.storyCellVM.story.tag ?? "notSet")")
                                .font(.footnote).fontWeight(.bold)
                                .padding(.horizontal, 17)
                                .offset(x: 0, y: 22)
                        }
                        Spacer()
                        VStack(spacing: 0) {
                            
                            
                            ZStack {
                                
                                Circle().foregroundColor(.pink)
                                    .frame(width: 40, height: 40)
                                
                                if self.storyCellVM.story.businessImage != nil {
                                    AsyncImage(url: URL(string:
                                        self.storyCellVM.story.businessImage!)!, cache: self.cache, placeholder: Text("loading..."))
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: 35, height: 35)
                                        .clipShape(Circle())
                                }
                                
                            }.padding(5)
                            
                            VStack(alignment: .center, spacing: 0) {
                                
                                Text(self.storyCellVM.story.location ?? "not set").font(.caption)
                            }
                        }
                            
                        .padding(.trailing, 12)
                        
                    }
                    //.padding(.top, 12)
                     
                }.frame(height: 240)
            }
            .cornerRadius(12, corners: [.topLeft, .topRight])
            .background(
                RoundedRectangle(cornerRadius: 12)
                    
                    .foregroundColor(.white)
                    .shadow(
                        color: Color.gray,
                        radius: 4,
                        x: 2,
                        y: 2
                )
            )
                
                .frame(width: geometry.size.width * 0.92, height: geometry.size.height * 0.9, alignment: .center)
                .padding()
                .cornerRadius(8)
                .onTapGesture(perform: {
                    self.presentDetailsModal.toggle()
                })
                .sheet(isPresented: self.$presentDetailsModal) {
                    StoryDetailsView(isPresented: self.$presentDetailsModal, storyCellVM: self.storyCellVM)
            }
        }
        
    }
}
//struct StoryListCard_Previews: PreviewProvider {
//    static var previews: some View {
//        
//        StoryListCard(storyCellVM: storyCellVM, cache: cache)
//    }
//}


struct StoryListCard_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
