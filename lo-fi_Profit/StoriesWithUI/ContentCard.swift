
//
//  StoryListCard.swift
//  lo-fi_Profit
//
//  Created by Micaela Cavallo on 04/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//


import SwiftUI

struct StoryListCard: View {
    var imageString: String = "typewriter"
    var title: String
    var text: String
    var tag: String
    //change this to an array that will check for how many tags are present.
    //need to count string characters are arrange stacks accordingly.
    var author: String
    var authorPic : String
    // private let range = text.0..<text.50
    // get a substring that spans for about 3 lines plus and elipsis (...)
    var businessLocation : String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            
            ZStack {
                Image(imageString)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(maxHeight: 250)
                    .clipShape(Rectangle())
                
                
                AuthorNameOverlay(author: author)
                
            }
            VStack(alignment: .leading, spacing: 0) {
                Text(title)
                    .font(.title)
                    .padding(.horizontal)
                    .padding(.top, 14)
                    .padding(.bottom, 25)
                
                
                Text(text)
                    .font(.body)
                    .opacity(0.8)
                    .padding(.horizontal)
                
                HStack {
                    VStack {
                        
                        Text("#\(tag)")
                            .font(.footnote).fontWeight(.bold)
                            .padding(.horizontal, 17)
                        .offset(x: 0, y: 22)
                    }
                    Spacer()
                    VStack(spacing: 0) {
                        ShopProfileZStack(authorPic: authorPic)
                            .padding(5)
                        
                        VStack(alignment: .center, spacing: 0) {
                            
                            Text(businessLocation).font(.caption)
                            //Text("Germany").font(.caption)
                        }
                    }
                        
                    .padding(.trailing, 12)
                    
                }
                //.padding(.top, 12)
                
                
                
            }.frame(height: 240)
        }
        .cornerRadius(12, corners: [.topLeft, .topRight])
        .background(
            RoundedRectangle(cornerRadius: 12)
                
                .foregroundColor(.white)
                .shadow(
                    color: Color.gray,
                    radius: 4,
                    x: 2,
                    y: 2
            )
        )
            
            .frame(width: 340, height: 470)
            .padding()
            .cornerRadius(8)
        
    }
}


struct StoryListCard_Previews: PreviewProvider {
    static let img = "typewriter"
    static let img2 = "redBike"
    static let img3 = "publicMarket"
    static let text = "A nebula is an interstellar cloud of dust, hydrogen, helium and other ionized gases."
    //Originally, nebula was a name for any diffuse astronomical object, including galaxies beyond the Milky Way.
    
    static var previews: some View {
        Group {
            StoryListCard(imageString: img, title: "A super interesting Title That's also kinda long", text: text, tag: "RefugeeEmployer", author: "Oliver Twist", authorPic: "typewriter", businessLocation:"Berlin, Germany")
            StoryListCard(imageString: img2, title: "Delightful Rides", text: text, tag: "BlackOwnedBusiness",  author: "Shakespeare", authorPic: "happyDog", businessLocation: "Colorado, USA")
            StoryListCard(imageString: img3,title: "Market With a Twist", text: text, tag: "FemaleOwnedBusiness", author: "Simone de Beauvoir", authorPic: "bluePortrait", businessLocation: "Paris, France")
            StoryListCard(title: "Flava letter soup", text: text, tag: "NeuroAtypicalFriendly", author: "Tim C.", authorPic: "happyDog", businessLocation: "Bologna, Italy")
        }
        .previewLayout(.device)
    }
    
}
