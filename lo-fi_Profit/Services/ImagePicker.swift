//
//  ImagePicker.swift
//  lo-fi_Profit
//
//  Created by cihan on 07/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI
import Firebase
import FirebaseStorage

struct ImagePicker: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentationMode
    @Binding var image: UIImage?
    
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        return picker
    }
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<ImagePicker>) {

    }
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        let parent: ImagePicker
        
        init(_ parent: ImagePicker) {
            self.parent = parent
        }
        
        // MARK: this controller will upload to Firebase Storage and create an url
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            let imageName = UUID().uuidString
            let uiImage = info[.originalImage] as! UIImage
            let imageReference = Storage.storage().reference().child("/Storie Pictures").child(imageName)
            parent.image = uiImage
//            {
//                parent.image = uiImage
//            }
            imageReference.putData(uiImage.jpegData(compressionQuality: 1.0)!, metadata: nil) { (_, error) in
                
                if error != nil {
                    print(error?.localizedDescription as Any)
                    return
                }
                imageReference.downloadURL(completion: {(url, error) in
                    if error != nil {
                        print(error?.localizedDescription as Any)
                    }
                    guard let url = url else {
                        print("Image URL not Found")
                        return
                    }
                    print(url)
                    
                    let dataReference = Firestore.firestore().collection("stories").document()
                    let documentUID = dataReference.documentID
                    
                    let urlString = url.absoluteString
                    
                    
                })
                print("Successfully uploaded")
            }
            parent.presentationMode.wrappedValue.dismiss()
        }
    }
    
}

