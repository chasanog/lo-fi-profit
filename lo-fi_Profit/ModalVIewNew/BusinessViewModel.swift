//
//  BusinessViewModel.swift
//  lo-fi_Profit
//
//  Created by Florent Frossard on 13/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct Business {
   
    var image : String
    var title : String
}
	
struct BusinessViewModel: Identifiable {
    
    var id = UUID()
    var business : Business
    
    var image : String {
        return business.image
    }
    
    var title : String {
        return business.title
    }
}

class BusinessData : ObservableObject {
   
    @Published var businessData : [BusinessViewModel]
    
    init(){
         businessData = [
        BusinessViewModel(business: Business(image: "Emma", title: "Emma's Torch")),
        BusinessViewModel(business: Business(image: "ChangePlease", title: "Change Please")),
        BusinessViewModel(business: Business(image: "ShadesTour", title: "Shades Tour"))
        ]
    }
}
