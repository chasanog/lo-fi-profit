//
//  CardDetailView.swift
//  Flavah
//
//  Created by cihan on 14/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct CardDetailView: View {
    
    @State private var viewState = CGSize.zero
    @State private var showFull = false
    @State private var showingExplore = true
    
//    @EnvironmentObject var businessData: BusinessData
    @ObservedObject var sbListVM = SocialBusinessListViewModel()

    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                Text("Value: \(self.viewState.height)")
                    
                    VStack {
                        Image(systemName: "chevron.down")
                        
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack {
                                ForEach(self.sbListVM.sbCellVM) { sbCellVM in
                                    VStack{
                                        oneSB(sbCellVM: sbCellVM)
                                    }
                                }
                            }
                        }
                        Spacer()

                    }
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
                    .cornerRadius(30)
                    .shadow(radius: 20)
                    .offset(x: 0, y: 300)
                    .offset(y: self.viewState.height)
                    .gesture(
                        // anchoring the drag position of the card
                        DragGesture()
                            .onChanged { value in
                                self.viewState = value.translation
                                if self.showFull{
                                    self.viewState.height += -150
                                }
                                // Maximum drag
                                if  self.viewState.height < -150 {
                                    self.viewState.height = -150
                                }

                        }
                        .onEnded { value in
                            
                            if (self.viewState.height < -50 && !self.showFull) || (self.viewState.height < -100 && self.showFull) {
                                self.viewState.height = -150
                                self.showFull = true
                            } else {
                                self.viewState = .zero
                                self.showFull = false
                            }
                        }
                    )
                }.animation(.spring())
        }
        
    }
}


struct CardDetailView_Previews: PreviewProvider {
    static var previews: some View {
        CardDetailView()
    }
}
