//
//  CardViewNew.swift
//  lo-fi_Profit
//
//  Created by Florent Frossard on 12/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct CardViewNew: View {
    
    @State private var viewState = CGSize.zero
    @State private var showFull = false
    @State private var chevron = "chevron.up"
    
//    @EnvironmentObject var businessData: BusinessData
    @ObservedObject var sbListVM = SocialBusinessListViewModel()

    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                Text("Value: \(self.viewState.height)")
                    
                    VStack {
                        Image(systemName: self.chevron)
                           
                        Text("Suggestions for you")
                            .font(.subheadline)
                            .multilineTextAlignment(.center)
                        
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack {
                                ForEach(self.sbListVM.sbCellVM) { sbCellVM in
                                    VStack{
                                        oneSB(sbCellVM: sbCellVM)
                                    }
                                }
                                if self.sbListVM.sbCellVM.count == 0 {
                                       HStack{
                                           Spacer()
                                       }
                                   }
                            }
                           
            }
                        Spacer()

                    }
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
                    .cornerRadius(30)
                    .shadow(radius: 20)
                    .offset(x: 0, y: geometry.size.height * 0.65)
                    .offset(y: self.viewState.height)
                    .gesture(
                        // anchoring the drag position of the card
                        DragGesture()
                            .onChanged { value in
                                self.viewState = value.translation
                                if self.showFull{
                                    self.viewState.height += -150
                                    self.chevron = "chevron.up"
                                }
                                // Maximum drag
                                if  self.viewState.height < -150 {
                                    self.viewState.height = -150
                                    self.chevron = "chevron.down"
                                }

                        }
                        .onEnded { value in
                            
                            if (self.viewState.height < -50 && !self.showFull) || (self.viewState.height < -100 && self.showFull) {
                                self.viewState.height = -150
                                self.showFull = true
                            } else {
                                self.viewState = .zero
                                self.showFull = false
                            }
                        }
                    )
                }.animation(.spring())
        }
        
    }
}

struct oneSB: View {
    @ObservedObject var sbCellVM: SocialBusinessCellViewModel
    @Environment(\.imageCache) var cache: ImageCache
    var body: some View {
//        HStack {
            VStack {
                Group {
                    if sbCellVM.sb.mainImage != nil {
                       AsyncImage(url: URL(string:
                        sbCellVM.sb.mainImage!)!, cache: self.cache, placeholder: Text("loading..."))
//                       .resizable()
                       .scaledToFit()
                       .frame(width: 150, height: 100)
                       .cornerRadius(20)
                   }
                    Text(sbCellVM.sb.title)
                }
               
            //                    Image(business.image)
            }
//        }
                
    }
}

struct CardViewNew_Previews: PreviewProvider {
    static var previews: some View {
        CardViewNew()
    }
}
