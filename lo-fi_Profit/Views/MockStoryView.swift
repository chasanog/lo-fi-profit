//
//  MockStoryView.swift
//  lo-fi_Profit
//
//  Created by cihan on 02/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI
import Firebase
//import FirebaseStorage

struct MockStoryView: View {
    @ObservedObject var storyListVM = StoryListViewModel()
    @EnvironmentObject var session: SessionStore
    
    
    @State var showImagePicker = false
    @State private var inputImage: UIImage?
    @State var image: Image?
    @State public var presentAddMewItem = false
    
    var body: some View {
        GeometryReader { geometry in
            NavigationView{
                VStack(alignment: .center, spacing: 0) {
                   Divider()
                    ScrollView{
                        ForEach(self.storyListVM.storyCellViewModels) { storyCellVM in
                           // StoryCell(storyCellVM: storyCellVM)
                            StoryListCard(storyCellVM: storyCellVM)
                                .frame(width: geometry.size.width, height: geometry.size.height * 0.8, alignment: .center)
                           
                        }
                        if self.storyListVM.storyCellViewModels.count == 0 {
                            HStack{
                                Spacer()
                            }
                        }
    // MARK: for future implementation available to add a new story.
    //                    if presentAddMewItem {
    //                        StoryCell(storyCellVM: StoryCellViewModel(story: Story(title: "", description: "", image: nil))) {story in
    //
    //                            self.storyListVM.addStory(story: story)
    //                            self.presentAddMewItem.toggle()
    //                        }
    //                        Button(action: {self.showImagePicker.toggle()}) {
    //                            Text("Select Photo")
    //                        }.sheet(isPresented: self.$showImagePicker, onDismiss: self.loadImage) {
    //                            ImagePicker(image: self.$inputImage)
    //                        }
    //                    }
    //                }
    //                if Auth.auth().currentUser != nil {
    //                    Button(action: { self.presentAddMewItem.toggle()}) {
    //                        HStack {
    //                            Text("Publish your Story")
    //                        }
    //                    }
                    }
                    .frame(width: geometry.size.width, height: geometry.size.height * 0.85, alignment: .center)
                    .frame(maxWidth: .infinity)
                
                    
                }.navigationBarTitle("Stories").frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                            
                        
                    }
        }
    }
    func loadImage() {
         guard let inputImage = inputImage else {return}
         return image = Image(uiImage: inputImage)
     }
}

struct MockStoryView_Previews: PreviewProvider {
    static var previews: some View {
        MockStoryView().environmentObject(SessionStore())
    }
}

struct StoryCell: View {
    @ObservedObject var storyCellVM: StoryCellViewModel
    @Environment(\.imageCache) var cache: ImageCache

    //empty collection
    var onCommit: (Story) -> (Void) = {_ in}

    
    var body: some View {
        VStack {
            TextField("Enter the Story title", text: $storyCellVM.story.title, onCommit: {
                self.onCommit(self.storyCellVM.story)
            })
                .font(.headline)
                .fixedSize(horizontal: false, vertical: true)
            TextField("Enter the Story Description", text: $storyCellVM.story.description, onCommit: {
                self.onCommit(self.storyCellVM.story)
            })
                .font(.footnote)
    
            if $storyCellVM.story.image.wrappedValue != nil {
                AsyncImage(url: URL(string: String($storyCellVM.story.image.wrappedValue!))!, cache: self.cache, placeholder: Text("Loading..."))
                    .aspectRatio(contentMode: .fit)
            }
        }
        
    }
    
 
}

