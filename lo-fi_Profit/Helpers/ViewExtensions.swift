//
//  ViewExtensions.swift
//  Flava
//
//  Created by Micaela Cavallo on 07/06/2020.
//  Copyright © 2020 cavallo.classes. All rights reserved.
//

import SwiftUI

extension View {
    // function for CornerRadius struct
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

/// Custom shape with independently rounded corners
struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
