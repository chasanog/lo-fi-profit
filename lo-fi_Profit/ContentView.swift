//
//  ContentView.swift
//  lo-fi_Profit
//
//  Created by cihan on 08/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI
import MapKit

struct ContentView: View {
    
    
    @EnvironmentObject var session: SessionStore
    
    @State private var exploreModalShown = true
    @State private var exploreOffsetModal = CGSize(width: 0, height: UIScreen.main.bounds.height - 100)
    @State private var modalView_shown = false
    @State private var offsetModal = CGSize(width: 0, height: 1000)

//    @State private var closedIsAvailable : 
    @State private var positionOfCard = ModalStates.closed
    @State private var positionOfExploreCard = ModalStates.smallAppear

    
    //getting User data from Firebase
    func getUser() {
        session.listen()
    }
    
    
    //this code can be used to change the tab bar color when we decide a color for it.
//    init() {
//        UITabBar.appearance().barTintColor = UIColor.systemTeal
//          }
    
    var body : some View {
        
        TabView() {
            
            MockStoryView().tabItem {
            Image(systemName: "doc.richtext")
                .font(.title)
                Text("Stories").font(.body)
            }
            
            FinalMapView().tabItem {
                       Image(systemName: "map")
                           .font(.title)
                Text("Explore").font(.body)
                       }
            
            ProfileView().tabItem {
            Image(systemName: "person")
                .font(.title)
                Text("Profile").font(.body)
            }
            
        }
        .onAppear(perform: getUser)
        .accentColor(Color.green)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(SessionStore())
    }
}
