//
//  Colors.swift
//  lo-fi_Profit
//
//  Created by Михаил on 01.06.2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

extension Color {
    
    static let bgColor = Color("background")
    static let fgColor = Color("foreground")
}

//struct Colors_Previews: PreviewProvider {
//    static var previews: some View {
//        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
//    }
//}
