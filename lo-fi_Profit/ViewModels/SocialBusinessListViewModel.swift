//
//  StoryListViewModel.swift
//  lo-fi_Profit
//
//  Created by cihan on 02/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import Combine

class SocialBusinessListViewModel: ObservableObject {
    @Published var sbRepository = SBRepository()
    @Published var sbCellVM = [SocialBusinessCellViewModel]()
    
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        sbRepository.$socialBusiness
            .map { socialBusiness in
                socialBusiness.map { socialBusiness in
                SocialBusinessCellViewModel(sb: socialBusiness)
            }
        }
        .assign(to: \.sbCellVM, on: self)
        .store(in: &cancellables)
    }
    func addSB(sb: SocialBusiness) {
        sbRepository.addSB(sb)
    }
}
