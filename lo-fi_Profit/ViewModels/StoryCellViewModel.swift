//
//  StoryCellViewModel.swift
//  lo-fi_Profit
//
//  Created by cihan on 02/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import Combine

class StoryCellViewModel: ObservableObject, Identifiable {
    @Published var storyRepository = StoryRepository()
    
    @Published var story: Story
    
    var id = ""
    
    private var cancellable = Set<AnyCancellable>()
    
    
    init(story: Story) {
        self.story = story
        
        $story
            .compactMap { story in
                story.id
        }
        .assign(to: \.id, on: self)
        .store(in: &cancellable)
        
        $story
            .dropFirst()
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .sink { story in
                self.storyRepository.updateStory(story)
        }
    .store(in: &cancellable)
    }
}
