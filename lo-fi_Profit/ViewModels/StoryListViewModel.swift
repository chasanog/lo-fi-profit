//
//  StoryListViewModel.swift
//  lo-fi_Profit
//
//  Created by cihan on 02/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import Combine

class StoryListViewModel: ObservableObject {
    @Published var storyRepository = StoryRepository()
    @Published var storyCellViewModels = [StoryCellViewModel]()
    
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        storyRepository.$stories
            .map { stories in
                stories.map { story in
                StoryCellViewModel(story: story)
            }
        }
        .assign(to: \.storyCellViewModels, on: self)
        .store(in: &cancellables)
    }
    
    func addStory(story: Story) {
        storyRepository.addStory(story)
//        let storyVM = StoryCellViewModel(story: story)
//        self.storyCellViewModels.append(storyVM)
    }
}
