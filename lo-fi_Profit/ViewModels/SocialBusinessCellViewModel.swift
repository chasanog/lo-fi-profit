//
//  SocialBusinessCellViewModel.swift
//  lo-fi_Profit
//
//  Created by cihan on 04/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import Combine

class SocialBusinessCellViewModel: ObservableObject, Identifiable {
    @Published var sbRepository = SBRepository()
    
    @Published var sb: SocialBusiness
    
    var id = ""
    
    private var cancellable = Set<AnyCancellable>()
    
    init(sb: SocialBusiness) {
        self.sb = sb
        
        $sb
            .compactMap { sb in
                sb.id
                
        }
        .assign(to: \.id, on: self)
    .store(in: &cancellable)
        
        $sb
        .dropFirst()
        .debounce(for: 0.8, scheduler: RunLoop.main)
            .sink{ sb in
                self.sbRepository.updateSB(sb)
                
        }.store(in: &cancellable)
    }
    
}
