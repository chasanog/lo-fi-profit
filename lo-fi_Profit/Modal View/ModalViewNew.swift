//
//  ModalViewNew.swift
//  lo-fi_Profit
//
//  Created by Михаил on 15.06.2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct ModalViewNew <Content: View> : View {
    @Environment(\.presentationMode) var presentationMode
    @GestureState private var dragState = DragState.inactive

    @Binding var selectedPin: BusinessAnnotation?
    @Binding var isShown : Bool
    var closedIsAvailable : Bool
    var modalHeight : CGFloat = 1800
    @Binding var positionOfCard : ModalStates
    @Binding var offsetModal : CGSize
//        = ( positionOfCard == .middle ? CGSize(width: 0, height: positionOfCard.changeOffset(modalHeight: 800)) : CGSize(width: 0, height: UIScreen.main.bounds.height - 800))
    @State private var newPosition : CGSize =  CGSize(width: 0, height: 800)

//    @State var cardState = [ ModalStates.closed, ModalStates.smallAppear, ModalStates.middle, ModalStates.fullScreen]
    
    var content: () -> Content

    var body: some View {
        let drag = DragGesture(minimumDistance: 0, coordinateSpace: .global)
//            .updating($dragState) { drag, state, transaction in
//    state = .dragging(translation: drag.translation)
//}
            .onChanged { gesture in
//                while gesture.translation.height + self.newPosition.height < UIScreen.main.bounds.height - self.modalHeight {
//                while self.offsetModal.height > -100 {
                print(self.newPosition.height, "f")
                    self.offsetModal.height = gesture.translation.height + self.newPosition.height
                    print(UIScreen.main.bounds.height)
                    print(self.offsetModal.height)
//                }
                
//                }
            }
            .onEnded(onDragEnded)
            return Group {
                ZStack {
                    if isShown {
//                Background
                        if closedIsAvailable {
                            Spacer()
                            .edgesIgnoringSafeArea(.all)
                            .frame(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                            .background(isShown ? Color.black.opacity(0.5 * fraction_progress(lowerLimit: 0, upperLimit: Double(modalHeight), current: Double(dragState.translation.height), inverted: true)) : Color.clear)
                            .animation(.interpolatingSpring(stiffness: 200.0, damping: 20.0, initialVelocity: 10.0))
                            .gesture(
                                TapGesture()
                                    .onEnded { _ in
                                        if self.closedIsAvailable {
                                            self.isShown = false
                                        } 
                                        
                                    }
                            )
                        }
//                Foreground
                        VStack {
                            Spacer()
                            ZStack(alignment: .topLeading) {
                                Color.bgColor
                            .frame(width: UIScreen.main.bounds.width, height: modalHeight)
                            .cornerRadius(10)
                        self.content()
                            .frame(width: UIScreen.main.bounds.size.width, height: modalHeight)
                            .clipShape(RoundedRectangle(cornerRadius: 10))

                    }
                            .offset(x: 0, y:
                                offsetModal.height)
                    .animation(.interpolatingSpring(stiffness: 100.0, damping: 30.0, initialVelocity: 10.0))
                    .gesture(drag)
                }
              }
            }
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
    }
    
    
    
    private func onDragEnded(drag: DragGesture.Value) {
        
        switch drag.translation.height + self.newPosition.height {
        case -500 ... 700:
            do {
                self.offsetModal.height = 600
                self.newPosition.height = self.offsetModal.height
            }
        case 700 ... 900:
            do {
                self.offsetModal.height = drag.translation.height + self.newPosition.height
                self.newPosition.height = self.offsetModal.height
            }
             
        case 900 ... 3000:
            do {
                print("isClosed")
                self.offsetModal.height = modalHeight
                self.newPosition.height = 800
                print(self.newPosition.height)
                self.isShown = false
                self.presentationMode.wrappedValue.dismiss()
//                isShown = false
            }
        default:
            do {
//                positionOfCard = .closed
//                self.offsetModal.height = drag.translation.height + self.newPosition.height
//                self.newPosition.height = self.offsetModal.height
            }
        }
//        if drag.translation.height + self.newPosition.height > UIScreen.main.bounds.height - self.modalHeight {
//            self.offsetModal.height = drag.translation.height + self.newPosition.height
//
//        } else if drag.translation.height + self.newPosition.height > 600 {
//            self.offsetModal.height = modalHeight
//        } else {
//            self.offsetModal.height = UIScreen.main.bounds.height - self.modalHeight
//        }
    }
}


struct ModalViewNew_Previews: PreviewProvider {
    static var previews: some View {
        ModalViewNew(selectedPin: .constant(BusinessAnnotation(title: "Mixa", subtitle: "a younger russian", coordinate: .init(latitude: 40.853204, longitude: 14.395573), image: "Photo")), isShown: .constant(true), closedIsAvailable: true, positionOfCard: .constant(ModalStates.middle), offsetModal: .constant(CGSize.zero)) {
            Card(selectedPin: .constant(BusinessAnnotation(title: "Mixa", subtitle: "a younger russian", coordinate: .init(latitude: 40.853204, longitude: 14.395573), image: "Photo")), offsetModal: .constant(CGSize(width: 0, height: 100)))

        }
    }
}
