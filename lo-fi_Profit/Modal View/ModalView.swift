//
//  ModalView.swift
//  lo-fi_Profit
//
//  Created by Михаил on 19.05.2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

enum ModalStates : CGFloat {
        case closed, middle, smallAppear, fullScreen
    
    func changeOffset(modalHeight: CGFloat) -> CGFloat {
        switch self {
        case .closed:
            return modalHeight
        case .middle:
            return UIScreen.main.bounds.height - 800
//            modalHeight / 2
        case .smallAppear:
            return 600
        case .fullScreen:
            return UIScreen.main.bounds.height * 0.2
        }
    }
}

struct ModalView1 <Content: View> : View {
    
    @GestureState private var dragState = DragState.inactive
    @Binding var isShown: Bool
    var closedIsAvailable : Bool
    @Binding var positionOfCard : ModalStates 
    @State var cardState = [ ModalStates.closed, ModalStates.smallAppear, ModalStates.middle, ModalStates.fullScreen]
//    @Binding var idModal : UUID
    
    var modalHeight: CGFloat = 800
    
    @State private var offsetModal : CGSize = CGSize(width: 0, height: ModalStates.middle.changeOffset(modalHeight: 800))
    @State private var newPosition : CGSize = CGSize(width: 0, height: 0)
    
    private func onDragEnded(drag: DragGesture.Value) {
        let dragThreshold = modalHeight * (2/3)
        switch drag.startLocation.y - drag.location.y {
            
//        }
//        switch offsetModal.height {
        case 0...1000:
            
            do {
            print("+", drag.startLocation.y - drag.location.y)
            if self.positionOfCard != ModalStates.fullScreen {
                print(cardState.firstIndex(of: self.positionOfCard)!)
                self.positionOfCard = cardState[cardState.firstIndex(of: self.positionOfCard)! + 1]
                offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
            } else {
                print("full screen")
                print(UIScreen.main.bounds.height)
            }
            
        }
            
        case -1000...0:
        do {
            if closedIsAvailable {
                print("-",  drag.startLocation.y - drag.location.y)
                if self.positionOfCard != ModalStates.closed {
                    print(cardState[cardState.firstIndex(of: self.positionOfCard)!])
                    self.positionOfCard = cardState[cardState.firstIndex(of: self.positionOfCard)! - 1]
                    print(self.positionOfCard)
                    offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
                    if self.positionOfCard == ModalStates.closed {
                        isShown = false
                        
                        print(isShown, "the modal Closed")
                    }
                } else {
                    print("closed")
                }
            } else {
                print("-",  drag.startLocation.y - drag.location.y)
                if self.positionOfCard != ModalStates.smallAppear {
                    print(cardState[cardState.firstIndex(of: self.positionOfCard)!])
                    self.positionOfCard = cardState[cardState.firstIndex(of: self.positionOfCard)! - 1]
                    print(self.positionOfCard)
                    offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
                } else {
                    print("closed")
                }
            }
            
        }
            
            
            
//        case 200...500:
//            self.positionOfCard = .fullScreen
//            offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
//        case 0...200:
//            self.positionOfCard = .smallAppear
//            offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
//        case -300...0:
//            if closedIsAvailable {
//                self.positionOfCard = .closed
//                offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
//            } else {
//                self.positionOfCard = .smallAppear
//                offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
//            }
//            self.positionOfCard = .closed
//            offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
        default:
            print("default")
            self.positionOfCard = .middle
            offsetModal.height = positionOfCard.changeOffset(modalHeight: modalHeight)
        }
//        if drag.predictedEndTranslation.height > dragThreshold || drag.translation.height > dragThreshold {
//            isShown = false
////            print(drag.translation.height)
//            print(drag.location.y)
////            self.offsetModal.height = drag.translation.height + self.newPosition.height
//            self.newPosition.height = self.offsetModal.height
//            print(newPosition.height)
//            if abs(self.offsetModal.height) > 100 {
//                positionOfCard = .fullScreen
//            }
//        }
    }
    
    var content: () -> Content
    
    var body: some View {
        let drag = DragGesture(minimumDistance: 0, coordinateSpace: .global)
            .updating($dragState) { drag, state, transaction in
                state = .dragging(translation: drag.translation)
            }
            .onChanged { gesture in
                self.offsetModal.height =
                    gesture.location.y
                self.newPosition.height = gesture.location.y
            }
            .onEnded(onDragEnded)
            return Group {
                ZStack {
                    if isShown {
                        //          Background
                        if closedIsAvailable {
                            Spacer()
                            .edgesIgnoringSafeArea(.all)
                            .frame(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                            .background(isShown && positionOfCard != .closed ? Color.black.opacity(0.5 * fraction_progress(lowerLimit: 0, upperLimit: Double(modalHeight), current: Double(dragState.translation.height), inverted: true)) : Color.clear)
                            .animation(.interpolatingSpring(stiffness: 200.0, damping: 20.0, initialVelocity: 10.0))
                            .gesture(
                                TapGesture()
                                    .onEnded { _ in
                                        if self.closedIsAvailable {
                                            self.isShown = false
                                        } else {
                                            self.positionOfCard = .smallAppear
                                        }
                                        
                                    }
                            )
                        }
                    
                    //          Foreground
                        VStack {
                            Spacer()
                            ZStack(alignment: .topLeading) {
                                Color.white.opacity(1.0)
                                    .frame(width: UIScreen.main.bounds.size.width, height: modalHeight)
                                    .cornerRadius(10)
                                    .shadow(radius: 5)
                                self.content()
                                    .padding(.bottom, 65)
                                    .frame(width: UIScreen.main.bounds.size.width, height: modalHeight)
                                    .clipShape(RoundedRectangle(cornerRadius: 10))
                                
                            }
                            .offset(x: 0, y: positionOfCard.changeOffset(modalHeight: modalHeight))
                            .animation(.interpolatingSpring(stiffness: 300.0, damping: 30.0, initialVelocity: 10.0))
                            .gesture(drag)
                        }
                    }
                }
                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                
        }
    }
}



func fraction_progress(lowerLimit: Double = 0, upperLimit: Double, current: Double, inverted: Bool = false) -> Double{
    var val: Double = 0
    if current >= upperLimit {
        val = 1
    } else if current <= lowerLimit {
        val = 0
    } else {
        val = (current - lowerLimit)/(upperLimit - lowerLimit)
    }
    
    if inverted {
        return (1 - val)
    } else {
        return val
    }
}

enum DragState {
    case inactive
    case dragging(translation: CGSize)
    
    var translation: CGSize {
        switch self {
        case .inactive:
            return .zero
        case .dragging(let translation):
            return translation
        }
    }
    
    var isDragging: Bool {
        switch self {
        case .inactive:
            return false
        case .dragging:
            return true
        }
    }
}

//this solution is harder and doesnt work as it should
// {
//struct Modal: Identifiable {
//    let id = UUID()
//    var content : AnyView
//    var position : ModalState = .base
//
//    var isFullScreenEnabled : Bool = false
//    var dragOffset : CGSize = .zero
//}
//
//enum ModalState: CGFloat {
//
//    case closed, partiallyRevealed, open, fullScreen
//    case base, backgrounded, occluded
//
//    func offsetFromTop() -> CGFloat {
//        switch self {
//        case .closed:
//            return UIScreen.main.bounds.height + 42
//        case .partiallyRevealed:
//            return UIScreen.main.bounds.height/1.8
//        case .open:
//            return 20
//        case .fullScreen, .base, .backgrounded:
//            return 0
//        case .occluded:
//            return 30
//        }
//    }
//}
//
//class ModalManager: ObservableObject {
//    @Published var modals: [Modal] = []
//
//    func fetchIndex(forID: UUID) -> Int { self.modals.firstIndex { $0.id == forID }! }
//    func fetchModal(forID: UUID) -> Modal? { self.modals.first { $0.id == forID } }
//    func isFirst(id: UUID) -> Bool { return self.modals.first!.id == id }
//
//    func fetchNext(forID: UUID) -> Modal? {
//        let index = fetchIndex(forID: forID)
//        if self.modals.indices.contains(index + 1) {
//            return self.modals[index + 1]
//        } else {
//            return nil
//        }
//    }
//
//    func fetchContent() {
//        modals =
//            [Modal(content: AnyView(
//                ZStack {
//                    Color.clear.opacity(0.5)
//                    Button(action: {
//                        self.modals[1].position = .partiallyRevealed
//                    }, label: {
//                        Text("Add modal")
//                    })
//                }
//            ), position: .partiallyRevealed),
//             Modal(content: AnyView(
//                ZStack {
//                    Color.red.opacity(0.1)
//                    Button(action: {
//                        self.modals[2].position = .partiallyRevealed
//                    }, label: {
//                        Text("Add modal")
//                    })
//                }
//
//             ), position: .partiallyRevealed),
//             Modal(content: AnyView(
//                ZStack {
//                    Color.orange.opacity(0.1)
//                    Button(action: {
//                        self.modals[3].position = .partiallyRevealed
//                    }, label: {
//                        Text("Add modal")
//                    })
//                }
//
//             ), position: .closed),
//             Modal(content: AnyView(Color.purple.opacity(0.1)), position: .closed)
//        ]
//    }
//}
//
//
//struct ModalView: View {
//
//    @EnvironmentObject var modalManager : ModalManager
//    @Binding var currentModal : Modal
//
//    var isActive: Bool {
//        if let nextModal = modalManager.fetchNext(forID: currentModal.id) {
//            return [.partiallyRevealed, .closed].contains(nextModal.position) && !self.modalManager.isFirst(id: currentModal.id)
//
//        } else {
//            return [.open, .partiallyRevealed, .closed, .fullScreen].contains(self.currentModal.position)
//        }
//    }
//
//    var body: some View {
//        return ModifiedContent(content: currentModal.content, modifier: ModalModifier(isActive: isActive, position: $currentModal.position, offset: $currentModal.dragOffset, isFullScreenEnabled: currentModal.isFullScreenEnabled, modalID: currentModal.id))
//    }
//}
//
//
//enum DragState {
//
//    case inactive
//    case dragging(translation: CGSize)
//
//    var translation: CGSize {
//        switch self {
//        case .inactive:
//            return .zero
//        case .dragging(let translation):
//            return translation
//
//        }
//    }
//
//    var isDragging: Bool {
//        switch self {
//        case .inactive:
//            return false
//        case .dragging:
//            return true
//        }
//    }
//}
//
//struct ModalModifier: ViewModifier {
//    @EnvironmentObject var modalManager: ModalManager
//    var isActive: Bool
//
//    @GestureState var dragState: DragState = .inactive
//    @Binding var position : ModalState
//    @Binding var offset : CGSize
//    @State var isFullScreenEnabled = false
//
//    var modalID: UUID
//    var animation: Animation {
//        Animation.interpolatingSpring(stiffness: 300.0, damping: 30.0, initialVelocity: 10.0)
//        .delay(self.position == .fullScreen ? 3 : 0)
//    }
//
//    var timer : Timer? {
//        return Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { timer in
//            if self.position == .open && self.dragState.translation.height == 0 && self.isFullScreenEnabled {
//                self.position = .fullScreen
//            } else {
//                timer.invalidate()
//            }
//        }
//    }
//
//    func body(content: Content) -> some View {
//        let drag = DragGesture()
//        .updating($dragState) { drag, state, transation in
//            state = .dragging(translation: (self.position != .fullScreen) ? drag.translation:.zero)
//        }
//        .onChanged {
//            self.offset = (self.position != .fullScreen) ? $0.translation:.zero
//        }
//        .onEnded(onDragEnded)
//
//        let nextModal = self.modalManager.fetchNext(forID: modalID)
//        let isBackgrounded = nextModal != nil ? ![.base, .closed, .partiallyRevealed].contains(nextModal!.position):false
//        let isFirst = modalManager.isFirst(id: modalID)
//
//        return ZStack(alignment: .top) {
//
//            Color.black.opacity(isActive && !isFirst ? 0 : 1)
//            ZStack {
//                Color.white.opacity(isActive && !isFirst ? 0 : 1)
//                content
//                    .offset(y: isFirst ? 42 : 0)
//            }
//            .mask(RoundedRectangle(cornerRadius: isActive ? 20 : isBackgrounded ? 15 : 0, style: .continuous))
//            .scaleEffect(x: isActive ? 1:isBackgrounded ? 0.9:1, y: isActive ? 1: isBackgrounded ? 0.9:1, anchor: .center)
//
//            Rectangle()
//                .fill(Color.black)
//                .opacity(isActive ? 0: isBackgrounded ? 0.3 : 0)
//        }
//        .edgesIgnoringSafeArea(.vertical)
//
//        .offset(y: isActive ? max(0, self.position.offsetFromTop() + self.dragState.translation.height) : 0)
//        .animation(isActive ? (self.dragState.isDragging ? nil : animation) : animation)
//        .gesture(isActive ? drag : nil)
//    }
//
//    private func onDragEnded(drag: DragGesture.Value) {
//        if position == .fullScreen {
//            return
//        }
//
//        let higherStop: ModalState
//        let lowerStop: ModalState
//
//        let nearestPosition: ModalState
//
//        let dragDirection = drag.predictedEndLocation.y - drag.location.y
//
//        let offsetFromTopOfView = position.offsetFromTop() + drag.translation.height
//
//        if offsetFromTopOfView <= ModalState.partiallyRevealed.offsetFromTop() {
//            higherStop = .open
//            lowerStop = .partiallyRevealed
//        } else {
//            higherStop = .partiallyRevealed
//            lowerStop = .closed
//        }
//
//        if (offsetFromTopOfView - higherStop.offsetFromTop()) < (lowerStop.offsetFromTop() - offsetFromTopOfView) {
//            nearestPosition = higherStop
//        } else {
//            nearestPosition = lowerStop
//        }
//
//        if dragDirection > 0 {
//            position = lowerStop
//        } else if dragDirection < 0 {
//            position = higherStop
//        } else {
//            position = nearestPosition
//        }
//
//    }
//}
//}

struct ModalView_Previews: PreviewProvider {
//    @Binding var isShown: Bool
    static var previews: some View {
    Text("Hello,")
    }
}
