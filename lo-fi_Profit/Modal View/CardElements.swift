//
//  CardElements.swift
//  lo-fi_Profit
//
//  Created by Михаил on 27.05.2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct SearchField: View {
    @State private var searchingTask : String = ""
    @Binding var isTapped : Bool
    var body: some View {
        ZStack {
            Rectangle()
                .frame(width: UIScreen.main.bounds.width, height: 80)
                .foregroundColor(.white)
            HStack {
                ZStack {
                RoundedRectangle(cornerRadius: 15)
                    .frame(width:UIScreen.main.bounds.width - 30, height: 40)
                    .foregroundColor(Color.black.opacity(0.2))
                HStack {
                    TextField("Search", text: $searchingTask)
                        .onTapGesture {
                            self.isTapped.toggle()
                        }
                    
                    Spacer()
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(.gray)
                }
                    Button(action: { }) {
                        Text("Search")
                    }
            }
            
            .frame(width:UIScreen.main.bounds.width - 45, height: 40)
            }
        }
    }
}

struct PhotoAndTitle: View {
    @State var selectedPin: BusinessAnnotation?
    @Environment(\.imageCache) var cache: ImageCache
    var body: some View {
        ZStack {
            Color.bgColor
            .frame(width: UIScreen.main.bounds.width, height: 200)
            VStack(alignment: .leading) {
                ZStack(alignment: .bottomLeading) {
                    Color.bgColor
                    ZStack {
                        
                        AsyncImage(url: URL(string: (selectedPin?.image)!)!, cache: self.cache, placeholder: Text("loading..."))
                        //                    .resizable()
                                            .scaledToFill()
                                            .frame(width: UIScreen.main.bounds.width, height: 180)
                                            .clipped()
                        LinearGradient(gradient: Gradient(colors: [Color.bgColor.opacity(0.3),  .bgColor]), startPoint: .top, endPoint: .bottom)
                    }
                    
                    
                }
                
                Spacer()
                ZStack {
                    Text(verbatim: selectedPin?.title ?? " NOT FOUND")
                        .bold()
                        .font(.largeTitle)
                        .foregroundColor(.fgColor)
                        .padding(10)
                    
                }
            }
        }
        .frame(width: UIScreen.main.bounds.width, height: 230)
    }
}

struct LocationPad : View {
    @Binding var offsetModal : CGSize
    @State private var buttonTapped = false
    @State private var isShown = false
    @State private var positionOfCard = ModalStates.closed
    @State private var selectedPin: BusinessAnnotation?
//    @State private var showingExplore = true
    var body : some View {
        ZStack (alignment: .leading) {
//            Rectangle()
//            .frame(width: UIScreen.main.bounds.width, height: 80, alignment: .center)
//            ZStack {
//                MapView(offsetModal: $offsetModal, buttonTapped: $buttonTapped, isShown: $isShown, positionOfCard: $positionOfCard, selectedPin: $selectedPin)
//                Image("MapImage")
//                .resizable()
//                .scaledToFill()
//                .frame(width: UIScreen.main.bounds.width, height: 80, alignment: .center)
//                .padding(0)
//                Color.bgColor.opacity(0.3)
//            }
            Color.bgColor
            .frame(width: UIScreen.main.bounds.width, height: 80)
            VStack (alignment: .leading) {
                Text("Address")
                    .font(.system(size: 25))
                    .bold()
                    .foregroundColor(.fgColor)
                HStack {
                    Text("via Tribunali, 250, 80139, Naples, Campania")
                    .foregroundColor(.fgColor)
            
                }
            }
            .padding(10)
        }
        .frame(width: UIScreen.main.bounds.width, height: 80)
//        .padding(10)
    }
}

struct WorkingHoursPad : View {
    var body : some View {
        ZStack (alignment: .leading) {
            Rectangle()
            .frame(width: UIScreen.main.bounds.width, height: 80, alignment: .center)
                .foregroundColor(.bgColor)
            VStack (alignment: .leading) {
                Text("Working hours")
                    .font(.system(size: 25))
                    .foregroundColor(.fgColor)
                    .bold()
                HStack {
                    Text("From 8:00 to 18:00")
                    .foregroundColor(.fgColor)
                    Text("Mo-Fr")
                    .foregroundColor(.fgColor)
                }
            }
            .padding(10)
        }
//        .padding(10)
    }
}

struct DescriptionPad : View {
    @State var selectedPin: BusinessAnnotation?
    @Environment(\.imageCache) var cache: ImageCache
    var body : some View {
        ZStack (alignment: .leading) {
            Rectangle()
            .frame(width: UIScreen.main.bounds.width, height: 160, alignment: .center)
            .foregroundColor(.bgColor)
            VStack (alignment: .leading) {
                Text("Beautiful cafe")
                    .font(.system(size: 25))
                    .bold()
                    .foregroundColor(.fgColor)
                    Text("Beautiful cafe with ")
                    .foregroundColor(.fgColor)
                    HStack {
                        ForEach(0 ..< 1) {_ in
                            AsyncImage(url: URL(string: (self.selectedPin?.image)!)!, cache: self.cache, placeholder: Text("loading..."))
//                                .resizable()
                                .scaledToFill()
                                .frame(width: 70, height: 70)
                                .clipShape(
                                    RoundedRectangle(cornerRadius: 10)
                            )
                    }
                }
            }
            .padding(10)
        }
    }
}

struct ContactPad: View {
    var colorsOfSocialSites : [Color] = [.green, .orange, .red, .blue]
    var logosOfSocialSites : [String] = ["T", "I", "R", "F"]
    
    var body: some View {
        ZStack (alignment: .leading){
            Rectangle()
                .frame(width: UIScreen.main.bounds.width, height: 120)
                .foregroundColor(.bgColor)
//                .foregroundColor(.white)
            VStack(alignment: .leading) {
                HStack {
                    VStack(alignment: .leading) {
                        HStack {
                            Text("Phone :")
                            .foregroundColor(.fgColor)
                            Text("+39 060 060 6000")
                            .foregroundColor(.fgColor)
                        }
                        HStack {
                            Text("Email :")
                            .foregroundColor(.fgColor)
                            Text("goodplace@gmail.com")
                            .foregroundColor(.fgColor)
                        }
                    }
                    Spacer()
                    Button(action: {
//                        there will be call button
                    }) {
                        ZStack {
                            RoundedRectangle(cornerRadius: 10)
                            .frame(width: 60, height: 30)
                                .foregroundColor(.blue)
                            Text("Call")
//                                .font(.)
                                .bold()
//                                .foregroundColor(.bgColor)
                                .foregroundColor(.white)
                        }
                        
                    }
                }
                
                
                HStack {
                    ForEach(0 ..< 4) { number in
                        ZStack {
                            Circle()
                                .frame(width: 40, height: 40)
                                .foregroundColor(self.colorsOfSocialSites[number])
                            Text(self.logosOfSocialSites[number])
                                .font(.title)
//                                .bold()
                                .foregroundColor(.white)
                        }
                    }
                }
            }
            .padding(10)
        }
        .frame(width: UIScreen.main.bounds.width, height: 120, alignment: .center)
    }
    
}

struct ReviewPad : View {
    var body: some View {
        ZStack (alignment: .leading) {
            Rectangle()
                .frame(width: UIScreen.main.bounds.width, height: 160, alignment: .center)
                .foregroundColor(.bgColor)
//                .foregroundColor(.white)
            VStack (alignment: .leading) {
//                    VStack {
                        HStack {
                        Text("Enjoy it a lot")
                            .foregroundColor(.fgColor)
                            .font(.system(size: 25))
                            .bold()
                        Spacer()
                        
                        ForEach(0..<5) { _ in
                            Image(systemName: "star.fill")
                                .foregroundColor(.gray)
                        }
//                        }
                            
                    }
                    Text("Beautiful cafe with ")
                    Spacer()
                HStack {
                    Text("39 days ago")
                    .font(.system(size: 10))
                    .foregroundColor(Color.gray.opacity(0.7))
                    .italic()
                    Spacer()
                    HStack {
                        Text("by:")
                        .font(.system(size: 10))
                        .foregroundColor(Color.gray.opacity(0.7))
                        
                        Text("Francesco Dell'Aglio")
                        .font(.system(size: 10))
                        .foregroundColor(Color.gray.opacity(0.7))
                        
                    }
                }
                
                    
//                }
            }
            .padding(10)
        }
        .frame(width: UIScreen.main.bounds.width, height: 160, alignment: .center)
    }
}

struct CardElements_Previews: PreviewProvider {
    @State var isTapped : Bool = true
//    @State var business : Business = Business(name: "Push me", id: UUID(), workingHours: ["Monday-Friday 10:00 - 20:00"], description: "AMG", contact: "00000000000", socialSites: "https://thereshouldbelink.us", address: "the whole world", longitude: 14.305573, latitude: 40.853294)
    static var previews: some View {
        Group {
//        VStack(spacing: 30) {
            SearchField(isTapped: .constant(true))

            PhotoAndTitle(selectedPin: BusinessAnnotation(title: "Naples", subtitle: "house of COVID 19", coordinate: .init(latitude: 40.853294, longitude: 14.305573), image: "lighstDeli"))

            WorkingHoursPad()

            ContactPad()

            LocationPad(offsetModal: .constant(CGSize(width: 0, height: 0)))
                .previewLayout(PreviewLayout.sizeThatFits)

            DescriptionPad()

            ReviewPad()
//                .previewLayout(PreviewLayout.sizeThatFits)
        }

        .previewLayout(PreviewLayout.sizeThatFits)
    }
}

//struct CardElements_Previews: PreviewProvider {
//    static var previews: some View {
//        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
//    }
//}
