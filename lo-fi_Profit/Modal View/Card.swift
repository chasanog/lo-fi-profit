//
//  Card.swift
//  lo-fi_Profit
//
//  Created by Михаил on 25.05.2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct Card: View {
    @State private var heightCard : CGFloat = 400
//    @Binding var idModal : UUID
    @Binding var selectedPin: BusinessAnnotation?
    @Binding var offsetModal: CGSize
    
    var body: some View {
        VStack(alignment: .leading) {
//            ScrollView(.vertical) {
            VStack (spacing: 0) {
//                PhotoAndTitle(idModal: $idModal)
                PhotoAndTitle(selectedPin: selectedPin)
                WorkingHoursPad()
                Divider()
                LocationPad(offsetModal: $offsetModal)
                Divider()
//                ContactPad()
//                Divider()
                DescriptionPad(selectedPin: selectedPin)
//                Divider()
//                ReviewPad()
//                }
                Spacer()
            }
        }
    }
}

struct ExploreCard : View {
    @State private var isTapped = false
    var body: some View {
        VStack {
            SearchField(isTapped: $isTapped)
            Divider()
            List {
                ForEach (0 ..< 5) {_ in
                    Text("Search result")
                    .frame(width: UIScreen.main.bounds.width, height: 60, alignment: .leading)
                    .foregroundColor(.fgColor)
                    .background(Color.bgColor)
                }
            }
        }
    }
}


struct Card_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            Card(selectedPin: .constant(BusinessAnnotation(title: "Place", subtitle: "Working now", coordinate: .init(latitude: 40.853204, longitude: 14.395573), image: "Photo")), offsetModal: .constant(CGSize(width: 0, height: 100)))
                .previewLayout(PreviewLayout.sizeThatFits)
            ExploreCard()
                .previewLayout(PreviewLayout.sizeThatFits)
        }
        
    }
}
