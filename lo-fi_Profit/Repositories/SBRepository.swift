//
//  SBRepository.swift
//  lo-fi_Profit
//
//  Created by cihan on 04/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class SBRepository: ObservableObject {
    
    
    let db = Firestore.firestore()
    
    @Published var socialBusiness = [SocialBusiness]()
    
    init() {
        loadData()
    }
    
    //adds new story
    func loadData() {
        
//        let userId = Auth.auth().currentUser?.uid
        db.collection("socialBusinessTemp")
//            .order(by: "title")
//        .whereField("userId", isEqualTo: userId)
            .addSnapshotListener { (querySnapshot, error) in
//                guard let documents = querySnapshot?.documents else {
//                    print("No documents")
//                    return
//                }
//                self.socialBusiness = documents.compactMap {
//                    queryDocumentSnapshot -> SocialBusiness? in
//                    return try? queryDocumentSnapshot.data(as: SocialBusiness.self)
//                }
            if let querySnapshot = querySnapshot {
                self.socialBusiness = querySnapshot.documents.compactMap { document in
                    do {
                        let x = try document.data(as: SocialBusiness.self)
//                        print(x)
                        return x
                    } catch {
                        print(error)
                    }
                    return nil
                }
            }
        }
        
    }
    
    func addSB(_ socialBusiness: SocialBusiness) {
        do {
            var addedSB = socialBusiness
            let currentUser = Auth.auth().currentUser
            if currentUser != nil {
                addedSB.userId = currentUser?.uid

                let _ = try db.collection("socialBusinessTemp").addDocument(from: addedSB   )
            }
        } catch {
            fatalError("Unable to encode story: \(error.localizedDescription)")
        }
    }
    
    func updateSB(_ sb: SocialBusiness) {
        if let sbID = sb.id {
            do {
                try db.collection("socialBusinessTemp").document(sbID).setData(from: sb)

            } catch {
                fatalError("Unable to encode story: \(error.localizedDescription)")
            }
        }
    }
}
