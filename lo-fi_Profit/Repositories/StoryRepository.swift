//
//  StoryRepository.swift
//  lo-fi_Profit
//
//  Created by cihan on 03/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class StoryRepository: ObservableObject {
    
    
    let db = Firestore.firestore()
    
    @Published var stories = [Story]()
    
    init() {
        loadData()
    }
    
    //adds new story
    func loadData() {
        
//        let userId = Auth.auth().currentUser?.uid
        db.collection("stories")
        .order(by: "createdTime")
//        .whereField("userId", isEqualTo: userId)
            .addSnapshotListener { (querySnapshot, error) in
            if let querySnapshot = querySnapshot {
                self.stories = querySnapshot.documents.compactMap { document in
                    do {
                        let x = try document.data(as: Story.self)
                        return x
                    } catch {
                        print(error)
                    }
                    return nil
                }
            }
        }
        
    }
    
    func addStory(_ story: Story) {
        do {
            var addedStory = story
            let currentUser = Auth.auth().currentUser
            if currentUser != nil {
                addedStory.userId = currentUser?.uid
            
                let _ = try db.collection("stories").addDocument(from: addedStory   )
            }
        } catch {
            fatalError("Unable to encode story: \(error.localizedDescription)")
        }
    }
    
    func updateStory(_ story: Story) {
        if let storyID = story.id {
            do {
                try db.collection("stories").document(storyID).setData(from: story)
            
            } catch {
                fatalError("Unable to encode story: \(error.localizedDescription)")
            }
        }
    }
}
