//
//  LocationManager.swift
//  lo-fi_Profit
//
//  Created by Mario Di Nitto on 28/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import MapKit

class LocationManager: NSObject, ObservableObject {
    
    private let locationManager = CLLocationManager()
   @Published var location: CLLocation? = nil
    
    @Published var userLatitude: Double = 0
    @Published var userLongitude: Double = 0
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
//        guard let location = locations.last else {return}
//        
//        userLatitude = location.coordinate.latitude
//        userLongitude = location.coordinate.longitude
//        self.location = location
    }
}
