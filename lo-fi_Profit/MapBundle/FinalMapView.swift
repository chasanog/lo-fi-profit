//
//  FinalMapView.swift
//  lo-fi_Profit
//
//  Created by Mario Di Nitto on 18/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI
import MapKit

struct FinalMapView: View {
    
    @State private var buttonTapped = false
//    @State private var selectedPlace: MKAnnotation?
    @State private var isShown = false
//    @State private var showingPlaceDetails = false
    @State private var showingExplore = true
    @State private var positionOfCard = ModalStates.closed
    @State private var offsetModal = CGSize(width: 0, height: 800)
//    @State private var idModal : UUID = Businesses().businesses[0].id
    @State private var selectedPin: BusinessAnnotation?
//    @State private var businesses: [BusinessAnnotation]?
    @ObservedObject var sbListVM = SocialBusinessListViewModel()
//    @State var pins: [BusinessAnnotation] = [
//        BusinessAnnotation(title: "Londong", subtitle: "Big Smoke", coordinate: .init(latitude: 51.509865, longitude: -0.118092))
//    ]
//    @State var sbCellVM: SocialBusinessCellViewModel
    
    var body: some View {
        ZStack {
            MapView(offsetModal: $offsetModal, buttonTapped: $buttonTapped, isShown: $isShown, positionOfCard: $positionOfCard, selectedPin: $selectedPin)
                CardViewNew()
                .edgesIgnoringSafeArea(.all)
                           
//            }
            VStack {
                HStack {
                    
                    Spacer()
                    
                    Button(action: {
                        self.buttonTapped.toggle()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.buttonTapped = false
                        }
                    }) {
                        Image(systemName: "location")
                            .resizable()
                            .frame(width: 25, height: 25)
                            .foregroundColor(Color(#colorLiteral(red: 0.2070863843, green: 0.7790430188, blue: 0.3490925431, alpha: 1)))
                            .overlay(RoundedRectangle(cornerRadius: 5)
                                .stroke(Color(red: 1, green: 1, blue: 1), lineWidth: 1)
                                .frame(width: 35, height: 35))
                            .background(RoundedRectangle(cornerRadius: 5)
                                .fill(Color.white)
                                .opacity(0.8)
                                .frame(width: 35, height: 35)
                                .shadow(radius: 3, x: 2 , y: 2))
                    }
                }
                .padding(EdgeInsets(top: 140, leading: 0, bottom: 0, trailing: 20))
                
                Spacer()
            }
            ModalViewNew(selectedPin: $selectedPin, isShown: $isShown, closedIsAvailable: true, positionOfCard: $positionOfCard, offsetModal: $offsetModal) {
                Card(selectedPin: self.$selectedPin, offsetModal: self.$offsetModal)
                .onAppear{
                        self.showingExplore.toggle()
                }.animation(.easeInOut(duration: 1))
                .onDisappear{
                    self.showingExplore.toggle()
                }
                .animation(.easeInOut(duration: 1))
            }
//            ModalView1(isShown: $showingPlaceDetails, closedIsAvailable: true, positionOfCard: $positionOfCard) {
//                Card(selectedPin: self.$selectedPin)
//            }
        }
    }
}


struct FinalMapView_Previews: PreviewProvider {
    static var previews: some View {
        FinalMapView()
    }
}




