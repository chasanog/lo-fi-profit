//
//  MapView.swift
//  lo-fi_Profit
//
//  Created by Mario Di Nitto on 28/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import MapKit
import SwiftUI

struct MapView: UIViewRepresentable {
    @Binding var offsetModal : CGSize
    @Binding var buttonTapped: Bool
//    @Binding var selectedPlace: MKAnnotation?
    @Binding var isShown: Bool
//    @Binding var showingExplore: Bool 
    @Binding var positionOfCard : ModalStates
//    @Binding var idModal : UUID
    
//    let businessesA = BusinessAnnotationConnected.requestMockData()
//    let businesses = BusinessAnnotation.requestMockData()
    @ObservedObject var sbListVM = SocialBusinessListViewModel()
    @Binding var selectedPin: BusinessAnnotation?
//    @Binding var pins: [BusinessAnnotation]
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.mapType = .standard
        mapView.showsUserLocation = true
        mapView.delegate = context.coordinator
        return mapView
    }
    
    func updateUIView(_ view: MKMapView, context: Context) {
//         Code for the button action
        if buttonTapped == true {
            if view.centerCoordinate.latitude != view.userLocation.coordinate.latitude || view.centerCoordinate.longitude != view.userLocation.coordinate.longitude {
                let region = MKCoordinateRegion(center: view.userLocation.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
                view.setRegion(region, animated: true)
            }
            else {
                return
            }
        }
        sbListVM.sbCellVM.forEach({ sbCellVM in
            view.addAnnotation(BusinessAnnotation(title: sbCellVM.sb.title, subtitle: sbCellVM.sb.title
                , coordinate: .init(latitude: sbCellVM.sb.lat, longitude: sbCellVM.sb.lon), image: sbCellVM.sb.mainImage!)
            )})
//        view.addAnnotations(pins)
        if let selectedPin = selectedPin {
            view.selectAnnotation(selectedPin, animated: false)
        }
        
//        addAnnotations(from: view)
//         Code for showing the pins
//        view.addAnnotations(businessesA)
//        view.addAnnotations(businesses)
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self, selectedPin: $selectedPin)
    }
    
    class Coordinator: NSObject, MKMapViewDelegate {
        var parent: MapView
        @Binding var selectedPin: BusinessAnnotation?
        
        init(_ parent: MapView, selectedPin: Binding<BusinessAnnotation?>) {
            self.parent = parent
            _selectedPin = selectedPin
        }
        
        func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
            guard (view.annotation as? BusinessAnnotation) != nil else {
                return
            }
            selectedPin = nil
        }
//        Code for showing your zoomed position(in meters) when the app is launched
        func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
            if let annotationView = views.first {
                if let annotation = annotationView.annotation {
                    if annotation is MKUserLocation {
                        let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
                        mapView.setRegion(region, animated: true)
                    }
                }
            }
        }
//        Code for customizing the pins
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            guard !(annotation is MKUserLocation) else {
                return nil
            }
            
            let identifier = "Business"
            
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.image = UIImage(named: "greenBubble")
                annotationView?.canShowCallout = false
                
//                annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        
        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            guard let pin = view.annotation as? BusinessAnnotation else {
               return
            }
            parent.isShown = false
            pin.action?()
            
            parent.selectedPin = pin
            parent.positionOfCard = ModalStates.closed
            parent.isShown = true
//            parent.isModal =
//            parent.positionOfCard = .middle
            parent.offsetModal = CGSize(width: 0, height: 800)
            print(pin.title)
            print(parent.offsetModal)
//            parent.selectedPlace = view.annotation
            print(parent.isShown)
        }
    }
}

