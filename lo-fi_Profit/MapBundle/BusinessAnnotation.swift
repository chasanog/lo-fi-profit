//
//  BusinessAnnotation.swift
//  lo-fi_Profit
//
//  Created by Mario Di Nitto on 28/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import MapKit

//class BusinessAnnotationConnected: NSObject, MKAnnotation {
//    var business: Business
//    var title: String?
//    var subtitle: String?
//    var longitude : Double
//    var latitude : Double
//    var id : UUID
//    var coordinate: CLLocationCoordinate2D
////    create the var business to collect them there directly
//    init(business: Business) {
////        super.init()
//        self.business = business
//        self.title = business.name
//        self.subtitle = business.address
//        self.latitude = business.latitude
//        self.longitude = business.longitude
//        self.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: self.latitude)!, longitude: CLLocationDegrees(exactly: self.longitude)!)
//        self.id = business.id
//    }
//    
////    init(title: String?, subtitle: String?, latitude: Double, longitude: Double, id: UUID) {
////
////        self.title = title
////        self.subtitle = subtitle
////        self.latitude = latitude
////        self.longitude = longitude
////        self.id = id
////        self.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: self.latitude)!, longitude: CLLocationDegrees(exactly: self.longitude)!)
////    }
//    
//    static func requestMockData() -> [BusinessAnnotationConnected] {
//        let businesses = Businesses().businesses
//        var annots : [BusinessAnnotationConnected] = []
//        for number in businesses {
//            let newBusiness =
//                BusinessAnnotationConnected(business: number)
////            BusinessAnnotationConnected(title: number.name, subtitle: number.description, latitude: number.latitudeC, longitude: number.longitudeC, id: number.id)
//            annots.append(newBusiness)
//        }
//        return annots
//        
//    }
//}

class BusinessAnnotation: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    let action: (() -> Void)?
    let image: String?
    
    init(title: String?, subtitle: String?, coordinate: CLLocationCoordinate2D, action: (() -> Void)? = nil, image: String) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.action = action
        self.image = image
    }
    
    
//    static func requestMockData()-> [BusinessAnnotation]{
//        return [
//            BusinessAnnotation(title: "Naples", subtitle: "house of COVID 19", coordinate: .init(latitude: 40.853294, longitude: 14.305573)),
//            BusinessAnnotation(title: "Mica", subtitle: "a nice girl", coordinate: .init(latitude: 40.753294, longitude: 14.535573)),
//            BusinessAnnotation(title: "Dmitry", subtitle: "a nice russian", coordinate: .init(latitude: 40.823294, longitude: 14.701573)),
//            BusinessAnnotation(title: "Cihan", subtitle: "a nice german/turkish", coordinate: .init(latitude: 40.853214, longitude: 14.365593)),
//            BusinessAnnotation(title: "Mario", subtitle: "the most beautiful italian guy", coordinate: .init(latitude: 40.857294, longitude: 14.385573)),
//            BusinessAnnotation(title: "Florent", subtitle: "a crazy french", coordinate: .init(latitude: 40.813294, longitude: 14.365573)),
//            BusinessAnnotation(title: "Mixa", subtitle: "a younger russian", coordinate: .init(latitude: 40.853204, longitude: 14.395573))
//        ]
//    }
}
