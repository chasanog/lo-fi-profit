//
//  BusinessProfileBuilderView.swift
//  lo-fi_Profit
//
//  Created by Micaela Cavallo on 10/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct BusinessProfileBuilderView: View {
    
    @State private var businessName = ""
    @State private var location = ""
    @State private var tags = ""
    @State private var businessType = ""
    @State private var contactNumber = ""
    @State private var openingHours = ""
    
    var body: some View {
        ScrollView {
            
            ZStack {
                Image("lighstDeli")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                
                
                
                Rectangle().foregroundColor(Color.green.opacity(0.4))
                VStack {
                    HStack {
                        Text("SET UP YOUR \n PROFILE").font(.title).fontWeight(.heavy).foregroundColor(Color.white)
                        Spacer()
                    }.offset(x: 25, y: -30)
                    
                    HStack {
                        Text("Join our #FLAVA \n businesses  \n and start sharing!")
                            
                            .font(.headline).fontWeight(.heavy).foregroundColor(Color.white.opacity(1))
                            .multilineTextAlignment(.trailing)
                        Spacer()
                    }.offset(x: 220, y: 60)
                }
                
                
                
            }.frame(height: 280)
                .clipped()
            
            VStack(alignment: .leading, spacing: 10) {
                VStack(alignment: .leading, spacing: 0) {
                    
                    ZStack {
                        Rectangle().foregroundColor(Color("darkPurple").opacity(0.3))
                            .frame(height: 40)
                            .cornerRadius(22)
                            .padding(.top)
                            .padding(.horizontal)
                        
                        HStack {
                            Text("BUSINESS' NAME")
                                .fontWeight(.black)
                                .foregroundColor(Color("darkPurple"))
                                .offset(x: 0, y: 10)
                            
                            Spacer()
                            Image(systemName: "person").font(.body)
                                .foregroundColor(Color("darkPurple"))
                                .offset(x: 0, y: 10)
                        }.padding(.horizontal, 30)
                        
                        
                    }
                    //.frame(height: 50)
                    
                    TextField("", text: $businessName)
                        .padding(.horizontal, 30).padding(.top, 20)
                    Rectangle()
                        .frame(height: 1)
                        .padding(.horizontal, 30)
                        .foregroundColor(Color("darkPurple").opacity(0.3))
                    
                }
                
                VStack(alignment: .leading, spacing: 0) {
                    
                    ZStack {
                        Rectangle().foregroundColor(Color("darkPurple").opacity(0.3))
                            .frame(height: 40)
                            .cornerRadius(22)
                            //.padding(.top)
                            .padding(.horizontal)
                        
                        HStack {
                            Text("LOCATION")
                                .fontWeight(.black)
                                .foregroundColor(Color("darkPurple"))
                            //     .offset(x: 0, y: 10)
                            
                            Spacer()
                            Image(systemName: "map").font(.body)
                                .foregroundColor(Color("darkPurple"))
                            // .offset(x: 0, y: 10)
                        }.padding(.horizontal, 30)
                        
                        
                    }
                    //.frame(height: 50)
                    
                    TextField("", text: $location)
                        .padding(.horizontal, 30).padding(.top, 20)
                    Rectangle()
                        .frame(height: 1)
                        .padding(.horizontal, 30)
                        .foregroundColor(Color("darkPurple").opacity(0.3))
                    
                }
                
                VStack(alignment: .leading, spacing: 0) {
                    
                    ZStack {
                        Rectangle().foregroundColor(Color("darkPurple").opacity(0.3))
                            .frame(height: 40)
                            .cornerRadius(22)
                            //.padding(.top)
                            .padding(.horizontal)
                        
                        HStack {
                            Text("TAGS")
                                .font(.headline)
                                .fontWeight(.black)
                                .foregroundColor(Color("darkPurple"))
                            //    .offset(x: 0, y: 10)
                            Text("EG #LGTBQ #REFUGEE")
                                .font(.caption).fontWeight(.bold)
                                .foregroundColor(Color("darkPurple"))
                            
                            Spacer()
                            Image(systemName: "heart").font(.body)
                                .foregroundColor(Color("darkPurple"))
                            //   .offset(x: 0, y: 10)
                        }.padding(.horizontal, 30)
                        
                        
                    }
                    //.frame(height: 50)
                    
                    TextField("", text: $tags)
                        .padding(.horizontal, 30).padding(.top, 20)
                    Rectangle()
                        .frame(height: 1)
                        .padding(.horizontal, 30)
                        .foregroundColor(Color("darkPurple").opacity(0.3))
                    
                }
                
                VStack(alignment: .leading, spacing: 0) {
                    
                    ZStack {
                        Rectangle().foregroundColor(Color("darkPurple").opacity(0.3))
                            .frame(height: 40)
                            .cornerRadius(22)
                            //  .padding(.top)
                            .padding(.horizontal)
                        
                        HStack {
                            Text("BUSINESS TYPE")
                                .fontWeight(.black)
                                .foregroundColor(Color("darkPurple"))
                            //    .offset(x: 0, y: 10)
                            
                            Text("EG BAKERY")
                                .font(.caption).fontWeight(.bold)
                                .foregroundColor(Color("darkPurple"))
                            Spacer()
                            Image(systemName: "wrench").font(.body)
                                .foregroundColor(Color("darkPurple"))
                            //  .offset(x: 0, y: 10)
                        }.padding(.horizontal, 30)
                        
                        
                    }
                    //.frame(height: 50)
                    
                    TextField("", text: $businessType)
                        .padding(.horizontal, 30).padding(.top, 20)
                    Rectangle()
                        .frame(height: 1)
                        .padding(.horizontal, 30)
                        .foregroundColor(Color("darkPurple").opacity(0.3))
                    
                }
                
                VStack(alignment: .leading, spacing: 0) {
                    
                    ZStack {
                        Rectangle().foregroundColor(Color("darkPurple").opacity(0.3))
                            .frame(height: 40)
                            .cornerRadius(22)
                            //.padding(.top)
                            .padding(.horizontal)
                        
                        HStack {
                            Text("CONTACT NUMBER")
                                .fontWeight(.black)
                                .foregroundColor(Color("darkPurple"))
                            //     .offset(x: 0, y: 10)
                            
                            Spacer()
                            Image(systemName: "phone").font(.body)
                                .foregroundColor(Color("darkPurple"))
                            //  .offset(x: 0, y: 10)
                        }.padding(.horizontal, 30)
                        
                        
                    }
                    //.frame(height: 50)
                    
                    TextField("", text: $contactNumber)
                        .padding(.horizontal, 30).padding(.top, 20)
                    Rectangle()
                        .frame(height: 1)
                        .padding(.horizontal, 30)
                        .foregroundColor(Color("darkPurple").opacity(0.3))
                    
                }
                VStack(alignment: .leading, spacing: 0) {
                    
                    ZStack {
                        Rectangle().foregroundColor(Color("darkPurple").opacity(0.3))
                            .frame(height: 40)
                            .cornerRadius(22)
                            //  .padding(.top)
                            .padding(.horizontal)
                        
                        HStack {
                            Text("OPENING HOURS")
                                .fontWeight(.black)
                                .foregroundColor(Color("darkPurple"))
                            //   .offset(x: 0, y: 10)
                            
                            Spacer()
                            Image(systemName: "clock").font(.body)
                                .foregroundColor(Color("darkPurple"))
                            //  .offset(x: 0, y: 10)
                        }.padding(.horizontal, 30)
                        
                        
                    }
                    //.frame(height: 50)
                    
                    TextField("", text: $openingHours)
                        .padding(.horizontal, 30).padding(.top, 20)
                    Rectangle()
                        .frame(height: 1)
                        .padding(.horizontal, 30)
                        .foregroundColor(Color("darkPurple").opacity(0.3))
                    
                }
            }
            //.background(Color("darkPurple").opacity(0.1))
            
        }.edgesIgnoringSafeArea(.top)
    }
}



struct BusinessProfileBuilderView_Previews: PreviewProvider {
    static var previews: some View {
        BusinessProfileBuilderView()
    }
}
