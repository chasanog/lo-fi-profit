//
//  ProfileAuthView.swift
//  lo-fi_Profit
//
//  Created by cihan on 29/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct Review: Identifiable {
    let id: Int
    let name, message: String
    let rating: Int
}


struct ProfileAuthView: View {
    @EnvironmentObject var sesseion: SessionStore
    
    @State var personalIsSelected = true
    
    @State private var showingImagePicker = false
    @State private var inputImage: UIImage?
    @State private var image: Image?
    
    let reviews : [Review] = [
        .init(id: 0, name: "Good Place", message: "sono io la persona che piu odi al mondo!", rating: 3),
        .init(id: 1, name: "Bad Place", message: "La verità è che ti manacano le palle di ammazzarlo tagliandogli la gola lentamente da lato a lato e guardarlo morire lentamente tra le tue braccia", rating: 4),
        .init(id: 2, name: "Standard Place", message: "sono io", rating: 2)
    ]
    
    var body: some View {
        ScrollView {
          
            BusinessProfileBuilderView()
            Button(action: sesseion.signOut) {
                Text("Sign Out").font(.title).foregroundColor(.blue)
                       }
        }.edgesIgnoringSafeArea(.top)
    }
}

struct UserPageScreen : View {
    @EnvironmentObject var session: SessionStore
    
    var body: some View {
        NavigationView {
            VStack {
                Text("Successfully logged in!")
            }
        }
    }
}

struct ProfileAuthView_Previews: PreviewProvider {

    static var previews: some View {
        Group {
            VStack {
                ProfileAuthView().environmentObject(SessionStore())
                UserPageScreen().environmentObject(SessionStore())
            }

        }

    }
}

