//
//  ProfileView.swift
//  lo-fi_Profit
//
//  Created by Mario Di Nitto on 28/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    @State var email: String = ""
    @State var password: String = ""
    @State var error: String = ""
    
    @State var showUserRegistration = false
    @State var coordinator: SignInWithAppleCoordinator?
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var session: SessionStore
    
    var isLoginInfoValid: Bool {
        if email.trimmingCharacters(in: .whitespaces).isEmpty || password.trimmingCharacters(in: .whitespaces).isEmpty {
            return false
        }
        return true
    }
    
    
    var body: some View {
        VStack{
            
            if session.session != nil {
                ProfileAuthView()
            } else {
                
                VStack(alignment: .leading, spacing: 30) {
                    Text("Welcome back to")
                        .fontWeight(.bold)
                        .font(.headline)
                        .foregroundColor(Color("darkGreen"))
                    
                    Text("FLAVA !")
                        
                        .fontWeight(.heavy)
                        .font(.largeTitle)
                        .foregroundColor(Color("darkGreen"))
                    
                    Text("Sign in and keep sharing\n #FLAVAFULL stories with the\nworld")
                        .fontWeight(.bold)
                        .font(.headline)
                        .foregroundColor(Color("darkGreen"))
                        .lineSpacing(30)
                }
                .frame(width: 320, height: 320)
                .offset(x: -45)
                .edgesIgnoringSafeArea(.top)
                .padding(.top, 50)
                
                VStack(alignment: .leading, spacing: 20){
                    
                    VStack(alignment: .leading, spacing: 15){
                        
                        TextField("      EMAIL ADDRESS", text: $email).font(.callout).foregroundColor(Color("darkGreen"))
                            .frame(height: 50).background(Color.green.opacity(0.3)).cornerRadius(22)
                        
                        SecureField("      ENTER PASSWORD", text: $password)
                            .frame(height: 50).background(Color.green.opacity(0.3)).cornerRadius(22)
                    }
                        
                    .padding(.horizontal, 50)
                    
                    
                    // button will make a call to the signIn function and try to login with the credentials given from the user
                    
                    Button(action: signIn) {
                        Text("Sign In").font(.headline)
                            .foregroundColor(.white)
                            .frame(width: UIScreen.main.bounds.width - 120)
                            
                            .padding()
                        
                    }
                    .background(isLoginInfoValid ? Color.orange : Color("darkGreen"))
                    .clipShape(Capsule())
                    .padding(.horizontal, 50)
                    .disabled(!isLoginInfoValid)
                    .frame(height: 50)
                }
                
                Text("Or").foregroundColor(Color.gray.opacity(0.9))
                    .padding(.top, 15)
                
                SignInWithAppleButton()
                    .frame(height: 50)
                    .clipShape(Capsule())
                    .padding(.horizontal, 50)
                    .onTapGesture {
                        self.coordinator = SignInWithAppleCoordinator()
                        if let coordinator = self.coordinator {
                            coordinator.startSignInWithAppleFlow {
                                print("Signed in with Apple")
                                self.presentationMode.wrappedValue.dismiss()
                            }
                        }
                        
                }
                
                // Text to display error message when user not existing
                if (error != "") {
                    Text(error)
                        .font(.system(size: 14, weight: .semibold))
                        .foregroundColor(.red)
                        .padding()
                }
                
                VStack(spacing: 20) {
                    HStack {
                        Text("Don't have an account ?")
                            .foregroundColor(Color.gray)
                        
                        Button(action: {
                            self.showUserRegistration = true
                        }) {
                            Text("Sign Up")
                                .foregroundColor(Color.green)
                                .fontWeight(.semibold)
                        }
                        .padding(.bottom, 5)
                        .sheet(isPresented: $showUserRegistration){
                            return UserRegistration().environmentObject(self.session)
                        }
                        
                    }
                    HStack {
                        Text("Forgot your password?")
                            .foregroundColor(Color.gray)
                        Button(action: {
                            
                        }) {
                            Text("Click Here")
                                .foregroundColor(Color.green)
                                .fontWeight(.semibold)
                            
                        }
                    }
                }
                .font(.callout)
                .padding(.vertical)
            }
        }.edgesIgnoringSafeArea(.top)
            .background(Color("darkPurple").opacity(0.1))
    }
    func signIn() {
        session.signIn(email: email, password: password) { (result, error) in
            if let error = error {
                self.error = error.localizedDescription
            } else {
                self.email = ""
                self.password = ""
            }
        }
    }
}



struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
