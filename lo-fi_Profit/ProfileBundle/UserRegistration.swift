//
//  UserRegistration.swift
//  lo-fi_Profit
//
//  Created by Mario Di Nitto on 28/05/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct UserRegistration: View {
    @State var username: String = ""
    @State var email: String = ""
    @State var password: String = ""
    @State var confirmPassword = ""
    @State var error: String = ""
    @EnvironmentObject var session: SessionStore
    @Environment(\.presentationMode) var presentationMode
    
    
    var isRegistrationValid: Bool {
        if email.trimmingCharacters(in: .whitespaces).isEmpty || password.trimmingCharacters(in: .whitespaces).isEmpty {
            return false
        }
        return true
    }
    
    var body: some View {
        VStack{
            VStack(alignment: .leading, spacing: 20) {
                VStack(alignment: .leading, spacing: 20) {
                    Text("JOIN FLAVA")
                        .fontWeight(.heavy)
                        .font(.title)
                        .foregroundColor(Color("darkPurple"))
                        .padding(.top, 100)
                    Text("Create an account and\nshare your #flavafull\n stories with the world")
                        .fontWeight(.bold)
                        .font(.headline)
                        .foregroundColor(Color("darkPurple"))
                        .lineSpacing(30)
                }
                //.padding(.top, 20)
                
                
                VStack(alignment: .leading, spacing: 20) {
                    TextField("      ENTER EMAIL", text: $email).frame(height: 50).background(Color.green.opacity(0.2)).cornerRadius(22)
                    
                    SecureField("      ENTER PASSWORD", text: $password).frame(height: 50).background(Color.green.opacity(0.2)).cornerRadius(22)
                    
                    SecureField("      CONFIRM PASSWORD", text: $confirmPassword).frame(height: 50).background(Color.green.opacity(0.2)).cornerRadius(22)
                    //.foregroundColor(Color("darkPurple"))
                }.font(.body).foregroundColor(.black)
                    .padding(.vertical, 30)
                
                HStack {
                    Spacer()
                    
                    Button(action: signUp) {
                        Text("SIGN UP")
                            .font(.body)
                            .foregroundColor(Color.white)
                         //   .frame(width: UIScreen.main.bounds.width - 120)
                        
                        
                    }
                    .frame(width: 150, height: 50, alignment: .center)
                    .background(isRegistrationValid ? Color.orange : Color("darkPurple"))
                    .clipShape(Capsule())
                    //.padding([.top, .bottom], 45)
                    .disabled(!isRegistrationValid)
                    Spacer()
                    
                    if (error != "") {
                        Text(error)
                            .font(.system(size: 14, weight: .semibold))
                            .foregroundColor(.red)
                            .padding()
                    }
                }
            }
            .frame(width: 320, alignment: .leading).padding(40)
           
        }.background(Color.green.opacity(0.1))
        .edgesIgnoringSafeArea(.top)
    }
    
    func signUp() {
        session.signUp(email: email, password: password) { (result, error) in
            if let error = error {
                self.error = error.localizedDescription
            } else {
                self.email = ""
                self.password = ""
                
            }
        }
    }
}




struct UserRegistration_Previews: PreviewProvider {
    static var previews: some View {
        UserRegistration()
    }
}

