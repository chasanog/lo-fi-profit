//
//  ProfileRow.swift
//  lo-fi_Profit
//
//  Created by Mario Di Nitto on 04/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import SwiftUI

struct ProfileRow: View {
    let review: Review
    
    var offColor = Color.gray
    var onColor = Color.yellow
    
    var body: some View {
        HStack {
            VStack(alignment: .leading){
                HStack{
                    Text(review.name)
                        .fontWeight(.bold)
                    
                    Spacer()
                    
                    HStack {
                        ForEach(1..<6) { star in
                            Image(systemName: "star.fill")
                                .foregroundColor(star > self.review.rating ? self.offColor : self.onColor)
                        }
                    }
                    .padding(.trailing)
                }
                
                
                Text(review.message)
                    .foregroundColor(.gray)
            }
            .padding(.leading, 5)
            
            
            
        }
    }
}

struct ProfileRow_Previews: PreviewProvider {
    static var previews: some View {
        ProfileRow(review: Review(id: 0, name: "Mariangela", message: "è una scansafatiche!", rating: 4))
    }
}
