//
//  Stories.swift
//  lo-fi_Profit
//
//  Created by cihan on 02/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct Story: Codable, Identifiable{
    //DocumentID will not try to read the id from FireStore
    @DocumentID var id: String?
    var title: String
    var description: String
    @ServerTimestamp var createdTime: Timestamp?
    var userId: String?
    var image: String?
    var tag: String?
    var businessImage : String?
    var author : String?
    var location : String?
}

#if DEBUG
let testDataStories = [
    Story(title: "This is the Title of a Story", description: "This could be one description"),
    Story(title: "This is another Title of a Story", description: "This could be another description of a Stroy"),
    Story(title: "This is the third Title of a Story", description: "This could be one description"),
    Story(title: "This is the fourth Title of a Story", description: "This could be another description of a Stroy")
]
#endif
