//
//  SocialBusiness.swift
//  lo-fi_Profit
//
//  Created by cihan on 04/06/2020.
//  Copyright © 2020 CmetH. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


struct SocialBusiness: Codable, Identifiable {
    //DocumentID will not try to read the id from FireStore
    @DocumentID var id: String?
    var title: String
//    var coordinate: CLLocationCoordinate2D
//    var city: String
//    var houseNumber: String
//    var address: [String]
//    var country: String
//    var postalcode: Int
//    var description: String
//    @ServerTimestamp var createdTime: Timestamp?
    var userId: String?
    var lat: Double
    var lon: Double
    var mainImage: String?
    var businessType: String?
    var phoneNumber: String?
    
//    enum CodingKeys: String, CodingKey {
//        case title
//        case lat
//        case lon
//        case mainImage
//        case address
//        case address
//        case description
//        case userId
//        case position
//    }
//    enum Address: String, CodingKey {
//        case city
//        case houseNumber
////        case country
////        case postalcode
//    }
//
//    enum Position: GeoPoint, CodingKey {
//        case lat
//        case lon
//    }
}

//extension SocialBusiness: Decodable {
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//
//        title = try values.decode(String.self, forKey: .title)
//
//        title = try values.decode(String.self, forKey: .title)
//        title = try values.decode(String.self, forKey: .title)
//
//        let position = try values.nestedContainer(keyedBy: Position.self, forKey: .position)
//        lat = try position.decode(Double.self, forKey: .lat)
//        lon = try position.decode(Double.self, forKey: .lon)
//
//        let address = try values.nestedContainer(keyedBy: Address.self, forKey: .address)
//        city = try address.decode(String.self, forKey: .city)
//        houseNumber = try address.decode(String.self, forKey: .houseNumber)
//    }
//}
//
//extension SocialBusiness: Encodable {
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
////        try container.encode(lat, forKey: .lat)
////        try container.encode(lon, forKey: .lon)
//
//        var address = container.nestedContainer(keyedBy: Address.self, forKey: .address)
//        try address.encode(city, forKey: .city)
//    }
//}
